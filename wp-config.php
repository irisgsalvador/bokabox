<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bokabox');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}YHXc!(E l<` f.sl,M`jjNRyDE2RY%:..YiG>{@L*%SK^oh~/^J oc5E_,l_J :');
define('SECURE_AUTH_KEY',  'srq+wVC jLL4L>Q-7` +E<Qw?m6bbxluVx1 eJZ+ilUz_[SNJ?zYc..X}0__#`tp');
define('LOGGED_IN_KEY',    'XHm !N]E8Q(+GY:5B>.^!Ir>s+2(*VC^pk:ktIOQoO{2fE@b,Az|AG)@w,6>6A0[');
define('NONCE_KEY',        'f#f 5kY{!Y&iPg4g/cF@zKaP;<Ix]|1%tiNXSaG$hghQqa@ksv/>+bs408)7V{Dv');
define('AUTH_SALT',        'PR?Lm,HS.UYqwUhivvcwS-K<JC80S%/34r}I~= dg7Rhbmah|YTUAfd~1W7BIp>m');
define('SECURE_AUTH_SALT', 'K>r4xqa*^C=@oP8zxF.`A{%rr-T/`R|g[29E}A^xVnfX 8:J c]c;M(Zs 1b*s/B');
define('LOGGED_IN_SALT',   '`ds^Y*M0mYZ,M7$14;l(1KlMBd>P#FfvJdUKNR`XakkMi2E. OVkbB.vqPfa/$GY');
define('NONCE_SALT',       'au*@/4*GxbIrzH:mJA[gv;^6.CELX:^}+h<#F7uJkN6h[42UZL!_wvn6>I&,S~p8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
