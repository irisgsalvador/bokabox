<?php
/*
 * Template Name: Menu
 */

get_header();

global $post;
?>


<div class="inner">
    <main id="main" class="container site-main clearfix">
        <div id="primary" class="content-area clearfix">
        <div class="title-bg"><h2 class="prod-title page-title"><?php the_title(); ?></h2></div>

        <div class="content-inner clearfix">
            <div class="content-page">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#weekly-meal-program">Weekly Meal Program</a></li>
                    <li><a data-toggle="tab" href="#dine-in-special">Dine-In Special</a></li>
                    <li><a data-toggle="tab" href="#ala-carte">Ala Carte</a></li>
                    <li><a data-toggle="tab" href="#dessert">Dessert</a></li>
                </ul>
                <div class="tab-content">
                    <div id="weekly-meal-program" class="tab-pane fade in active">
                        <?php wc_get_template_part('content', 'weekly-menu'); 
                        ?>
                    </div>
                    <div id="dine-in-special" class="tab-pane fade">
                        <?php echo do_shortcode('[product_category category="dine-in-special"]'); ?>
                    </div>
                    <div id="ala-carte" class="tab-pane fade">
                        <?php echo do_shortcode('[product_category category="ala-carte"]'); ?>
                    </div>
                    <div id="dessert" class="tab-pane fade">
                        <?php echo do_shortcode('[product_category category="dessert"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="secondary" class="widget-area secondary-right sidebar">
        <?php do_action('woocommerce_sidebar'); ?>
    </div>
    </main>
</div>
<?php get_footer(); ?>