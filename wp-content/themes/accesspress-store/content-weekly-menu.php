<?php 

$args = array(
'post_type' => 'product',
'product_cat' => 'weekly-meal-program',
'post_per_page' => 1,
'order' => 'DESC'
);
$product = new WP_Query($args);

?>

<?php
if ($product->have_posts()):
    while ($product->have_posts()): $product->the_post();
		$productid = get_the_ID();
		echo do_shortcode("[product_page id='$productid']");
		$mon = get_post_meta($productid, 'wpcf-program-monday', true);
		$tues = get_post_meta($productid, 'wpcf-program-tuesday', true);
		$wed = get_post_meta($productid, 'wpcf-program-wednesday', true);
		$thurs = get_post_meta($productid, 'wpcf-program-thursday', true);
		$fri = get_post_meta($productid, 'wpcf-program-friday', true);
		?>

			<div class="col-md-4 col-sm-6 col-xs-12 less-padding">
				<div class="col-bg">
					<div class="weekdays">Monday</div><?php echo $mon; ?>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12 less-padding">
				<div class="col-bg">
					<div class="weekdays">Tuesday</div><?php echo $tues; ?></div>
				</div>
			<div class="col-md-4 col-sm-6 col-xs-12 less-padding">
				<div class="col-bg">
					<div class="weekdays">Wednesday</div><?php echo $wed; ?></div>
				</div>

			<div class="col-md-4 col-sm-6 col-xs-12 less-padding">
				<div class="col-bg">
				<div class="weekdays">Thursday</div><?php echo $thurs; ?></div>
				</div>
			<div class="col-md-4 col-sm-6 col-xs-12 less-padding">
				<div class="col-bg">
				<div class="weekdays">Friday</div><?php echo $fri; ?></div>	
				</div>

<?php
    endwhile;
endif;
?>


<?php  ?>

