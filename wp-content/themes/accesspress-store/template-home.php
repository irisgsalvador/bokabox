<?php
/**
 * This is the front page code
 * Template Name: HomePage
 */
get_header();

//load slider
do_action('accesspress_slickslider');
?>

<?php if (is_active_sidebar('promo-widget-1')): ?>
    <section id="promo-section1">
        <div class="container">
            <div class="promo-wrap1">
                <div class="promo-product1 clearfix">
                    <div class="title-bg"><h2 class="prod-title">MEAL SERVICE / DISCOVER OUR MENU </h2></div>
                    <div class="product-left-column col-md-9 col-sm-8 col-xs-12" >
                    <?php dynamic_sidebar('promo-widget-1'); ?>
                    </div>
                    <div class="product-right-column col-md-3 col-sm-4 col-xs-12" >
                    <?php dynamic_sidebar('promo-widget-2'); ?>
                    </div>
                </div>
            </div>
            <a href=""><div class="cta-link">CHECK OUR MENU</div></a>
        </div>
    </section>
<?php endif; ?>

<section class="promotional-container parallax-bg">
    <div class="cta-overlay">
        <div class="container">
            <?php $post = get_page_by_title('About Boka Box');
            $about_excerpt = get_post_meta($post->ID, 'wpcf-page-excerpt', false);
            ?>
            <div class="content-left">
                <div class="title-bg"><h2 class="prod-title">THE BOKA BOX</h2></div>
                <?php echo $about_excerpt[0]; ?></div>
        </div>
    </div>
</section>

<section class="promo-section1">
    <div class="container">
        <div class="title-bg"><h2 class="prod-title">FROM THE BLOG / RECENT POSTS, EVENTS & MORE </h2></div>
        <ul>
            <?php $the_query = new WP_Query( 'posts_per_page=4' ); 
            while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

            <li class="col-md-3 col-sm-3 col-xs-6 recent-posts wow flipInY" data-wow-delay="1s">
                <div><?php echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' ); ?></div>
                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                <div>
                    <?php the_excerpt(__('(more…)')); ?>
                </div>
            </li>
            <?php 
            endwhile;
            wp_reset_postdata();
            ?>
        </ul>
        <div class="clearfix"></div>
        <a href=""><div class="cta-link">READ MORE</div></a>
    </div>
</section>

<?php if (is_active_sidebar('product-widget-1')): ?>
    <!-- This is Product 1 Section !-->
    <section id="product1" class="prod1-slider">
        <div class="container">
            <div class="title-bg"><h2 class="prod-title">TESTIMONIALS  </h2></div>
            <?php dynamic_sidebar('product-widget-1'); ?>
        </div>
    </section>
<?php endif; ?>


<?php if (is_active_sidebar('promo-widget-3')): ?>
    <section id="promo-section3">
        <div class="container">
            <div class="promo-wrap3">
                <div class="promo-product2">
                    <?php dynamic_sidebar('promo-widget-3'); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php
get_footer();
?>